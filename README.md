# Getting started with CSV Service

## Running

1) ### `docker-compose up -d --build`
2) ### `docker-compose up`

## Available Paths

### `http://localhost/docs` - OpenAPI documentation

## Additional information

### Example of age_range: 25-45