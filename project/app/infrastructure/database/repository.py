from datetime import date, datetime
from typing import Any, AsyncGenerator, Generic, Type

from sqlalchemy import Result, func, select, delete, update, insert, and_

from project.app.infrastructure.database.session import Session
from project.app.infrastructure.database.tables import ConcreteTable
from project.app.infrastructure.errors import NotFoundError, UnprocessableError


class BaseRepository(Session, Generic[ConcreteTable]):  # type: ignore
    """This class implements the base interface for working with database
    # and makes it easier to work with type annotations.
    """

    schema_class: Type[ConcreteTable]
    schema_class_join = Type[ConcreteTable]

    def __init__(self) -> None:
        super().__init__()

        if not self.schema_class:
            raise UnprocessableError(
                message=(
                    "Can not initiate the class without schema_class attribute"
                )
            )

    async def _multiple_save(self, payload: list[dict[str, Any]]):
        query = insert(
            self.schema_class
        ).values(
            payload
        )
        await self.execute(query)

    async def _filter(
            self,
            category: str,
            gender: str,
            birth_date: date,
            age: int,
            age_range: str,
            offset: int | None,
            limit: int | None
    ):
        optional_filter = []
        if category:
            optional_filter.append(getattr(self.schema_class, 'category') == category)
        if gender:
            optional_filter.append(getattr(self.schema_class, 'gender') == gender)
        if birth_date:
            optional_filter.append(getattr(self.schema_class, 'birthDate') == birth_date)
        if age:
            optional_filter.append(
                func.date_part('year', datetime.now()) - func.date_part('year', getattr(self.schema_class, 'birthDate')) == age
            )
        if age_range:
            age_range_list = age_range.split('-')
            age_result = func.date_part('year', datetime.now()) - func.date_part('year', getattr(self.schema_class, 'birthDate'))
            optional_filter.append(
                and_(
                    age_result >= int(age_range_list[0]),
                    age_result <= int(age_range_list[1])
                )
            )

        query = select(
            self.schema_class
        ).filter(
            *optional_filter
        ).offset(
            offset if offset else None
        ).limit(
            limit if limit else None
        )
        result: Result = await self.execute(query)
        schemas = result.scalars()

        for schema in schemas:
            yield schema
