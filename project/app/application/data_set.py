import csv
import os

import aiofiles
from datetime import datetime, date
from io import StringIO

from fastapi import UploadFile, BackgroundTasks
from fastapi.responses import FileResponse

from project.app.domain.data_set import DataSetRepository, DataSetUncommited, DataSetPublic

FIELDNAMES = ('category', 'firstname', 'lastname', 'email', 'gender', 'birthDate')


async def add_data(data_set: list, start: int, end: int):
    for i in range(start, end, 100):
        await DataSetRepository().create(
            data_set[i:i + 100]
        )


async def upload(file: UploadFile, background_tasks: BackgroundTasks):
    if '.csv' in file.filename:
        contents = await file.read()
        buffer = StringIO(contents.decode('utf-8'))
        csv_reader = csv.DictReader(buffer)
        csv_reader.fieldnames = FIELDNAMES
        result = list(csv_reader)[1:]

        buffer.close()
        await file.close()

        validate_data_set = []
        for ds in result:
            validate_data_set.append(DataSetUncommited(**ds))

        background_tasks.add_task(add_data, validate_data_set, 0, len(validate_data_set))

        return {"message": "Dataset uploaded in the background"}
    else:
        return {"message": "File must be .csv type"}


async def filter(
        category: str,
        gender: str,
        birth_date: date,
        age: int,
        age_range: str,
        skip: int,
        limit: int
) -> list[DataSetPublic]:
    data_set_filter = [
        DataSetPublic.model_validate(data)
        async for data in DataSetRepository().filter(
            category_=category,
            gender_=gender,
            birth_date_=birth_date,
            age_=age,
            age_range_=age_range,
            skip_=skip,
            limit_=limit
        )
    ]

    return data_set_filter


async def download(
        category: str,
        gender: str,
        birth_date: date,
        age: int,
        age_range: str
) -> FileResponse:
    os.makedirs(os.environ.get("FILES_ROOT"), exist_ok=True)
    file_path = f'{os.environ.get("FILES_ROOT")}/result_{datetime.now()}.csv'

    data_set_filter = [
        DataSetPublic.model_validate(data)
        async for data in DataSetRepository().filter(
            category_=category,
            gender_=gender,
            birth_date_=birth_date,
            age_=age,
            age_range_=age_range,
            skip_=None,
            limit_=None
        )
    ]

    async with aiofiles.open(file_path, 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=FIELDNAMES)

        await writer.writeheader()
        await file.seek(0, 2)

        for row in data_set_filter:
            await writer.writerow(row.model_dump(exclude={'id'}))

    return FileResponse(path=file_path, filename=os.environ.get("DOWNLOAD_FILE"), media_type='multipart/form-data')
