from datetime import date

from fastapi import APIRouter, Request, status, File, UploadFile, Query, BackgroundTasks
from fastapi.responses import FileResponse

from project.app.application.data_set import upload, filter, download

from fastapi_cache.decorator import cache

from project.app.domain.data_set import DataSetPublic

router = APIRouter(prefix='/data-sets', tags=['DataSets'])


@router.post("", status_code=status.HTTP_201_CREATED)
async def data_set_upload(
    _: Request,
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...)
) -> dict[str, str]:
    return await upload(file=file, background_tasks=background_tasks)


@router.get("", status_code=status.HTTP_200_OK)
@cache(expire=60)
async def data_set_filter(
    request: Request,
    skip: int = Query(0, alias='page', ge=0),
    limit: int = Query(10, le=100),
    category: str | None = None,
    gender: str | None = None,
    birth_date: date | None = None,
    age: int | None = None,
    age_range: str | None = None,
) -> list[DataSetPublic]:
    return await filter(
        category=category,
        gender=gender,
        birth_date=birth_date,
        age=age,
        age_range=age_range,
        skip=skip,
        limit=limit
    )


@router.get("/download", status_code=status.HTTP_200_OK)
async def data_set_download(
    request: Request,
    category: str | None = None,
    gender: str | None = None,
    birth_date: date | None = None,
    age: int | None = None,
    age_range: str | None = None,
) -> FileResponse:
    return await download(
        category=category,
        gender=gender,
        birth_date=birth_date,
        age=age,
        age_range=age_range
    )
