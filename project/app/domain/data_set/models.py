from datetime import date

from pydantic import Field

from project.app.infrastructure.models import InternalModel, PublicModel


# Public models
# ------------------------------------------------------
class _DataSetPublic(PublicModel):
    category: str = Field(description="OpenAPI desc")
    firstname: str = Field(description="OpenAPI desc")
    lastname: str = Field(description="OpenAPI desc")
    email: str = Field(description="OpenAPI desc")
    gender: str = Field(description="OpenAPI desc")
    birthDate: date = Field(description="OpenAPI desc")


class DataSetCreateRequestBody(_DataSetPublic):
    pass


class DataSetPublic(_DataSetPublic):
    id: int


# Internal models
# ------------------------------------------------------
class DataSetUncommited(InternalModel):
    category: str
    firstname: str
    lastname: str
    email: str
    gender: str
    birthDate: date


class DataSet(DataSetUncommited):
    id: int
