from datetime import date
from typing import AsyncGenerator

from project.app.infrastructure.database import BaseRepository, DataSetsTable
from .models import DataSet, DataSetUncommited


class DataSetRepository(BaseRepository[DataSetsTable]):
    schema_class = DataSetsTable

    async def create(self, csv: list[DataSetUncommited]):
        data = []
        for row in csv:
            data.append(row.model_dump())
        await self._multiple_save(data)

    async def filter(
            self,
            category_: str,
            gender_: str,
            birth_date_: date,
            age_: int,
            age_range_: str,
            skip_: int | None,
            limit_: int | None
    ) -> AsyncGenerator[DataSet, None]:
        async for instance in self._filter(
                category=category_,
                gender=gender_,
                birth_date=birth_date_,
                age=age_,
                age_range=age_range_,
                offset=(skip_ * limit_) if skip_ and limit_ else skip_,
                limit=limit_
        ):
            yield DataSet.model_validate(instance)
